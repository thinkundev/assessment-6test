﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using System.Net;
using Android.Util;
using System.Threading.Tasks;
using Android.Support.V7;
using Android.Net;
using Android.Content;
using System;
using Java.Lang;
using Java.IO;
using Android.Support.V7.App;

namespace Assessment6_Loaders
{
    [Activity(Label = "Assessment6_Loaders", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity, LoaderManager.ILoaderCallbacks
    {
        WeakReference<ProgressDialog> pb;

        static ImageView image;
        Bitmap bp = null;

		protected override void OnPause()
		{
            base.OnPause();
            ProgressDialog tmp_pb;
            pb.TryGetTarget(out tmp_pb);
            tmp_pb.Dismiss();
		}

		//        Loader ldr ;
		//        ManagerLoading manager = new ManagerLoading();
		protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            pb = new WeakReference<ProgressDialog>(new ProgressDialog(this));
            ProgressDialog tmp_pb;

            pb.TryGetTarget(out tmp_pb);
            tmp_pb.SetTitle("Downloader");
            if (LoaderManager?.GetLoader(0)?.IsStarted ?? false) {
                tmp_pb.Show();
            }
            tmp_pb.SetMessage("Downlowding image");

            image = FindViewById<ImageView>(Resource.Id.imageView1);
            LoaderManager.InitLoader(0, null, this);

            // Get our button from the layout resource,
            // and attach an event to it

            Button button = FindViewById<Button>(Resource.Id.myButton);
            button.Click += delegate {
                ProgressDialog tmp_pb1;

                var target_available = pb.TryGetTarget(out tmp_pb1);
                    

                //pb.Show();

                if (isNetConnected())
                {
                    if (target_available) {
                        tmp_pb1.Show();
                    }
                    LoaderManager.RestartLoader(0, null, this).ForceLoad();
//                    LoaderManager.GetLoader(0).ForceLoad();
                    Log.Debug("Debug", "in Connection is true");
                }else{
                   // pb.Dismiss();
                    if (target_available)
                    {
                        tmp_pb1.Dismiss();
                    }
                }

//                pb.Dismiss();
            };
        }
      
        public Loader OnCreateLoader(int id, Bundle args)
        {
            Log.Debug("Debug", "in OnCreateLoader");
            //MyAsyncLoader myAsyncLoader = new MyAsyncLoader(this);

            return new MyAsyncLoader(this);
        }

        public void OnLoaderReset(Loader loader)
        {
            Log.Debug("Debug", "in OnLoaderReset");
        }

        public void OnLoadFinished(Loader loader, Java.Lang.Object data)
        {
            Log.Debug("Debug","in OnLoadFinished");
            image.SetImageBitmap((Bitmap)data);

            ProgressDialog tmp_pb;

            pb.TryGetTarget(out tmp_pb);
            tmp_pb.Dismiss();
        }


        
        private class MyAsyncLoader : AsyncTaskLoader
        {
            string url = "http://www.effigis.com/wp-content/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg";
            public MyAsyncLoader(Context context) : base(context)
            {
                Log.Debug("Debug", "in MyAsyncLoader Constructor");
            }

            public override Java.Lang.Object LoadInBackground()
            {
                Log.Debug("Debug", "in LoadInBackground ");
                Bitmap bmp = null;
                using (var webClient = new WebClient())
                {
                    var bytes = webClient.DownloadData(url);
                    if (bytes != null)
                    {
                        bmp = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                    }
                }
                Log.Debug("Image", bmp.ToString());



                return bmp;
            }

        }










        //This function checks for all the connections (if any) and will return false if 
        public bool isNetConnected()
        {
            ConnectivityManager cm =
                (ConnectivityManager)this.GetSystemService(Context.ConnectivityService);

            Network[] activeNetwork;
            bool isConnected = false;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {

                activeNetwork = cm.GetAllNetworks();
                NetworkInfo networkInfo;

                foreach (Network mNetwork in activeNetwork)
                {
                    networkInfo = cm.GetNetworkInfo(mNetwork);

                    if (networkInfo.GetState().Equals(NetworkInfo.State.Connected))
                    {
                        isConnected = true;
                        return true;
                    }
                }
            }
            if (isConnected)
            {
                try
                {
                    Toast.MakeText(this, "Network is connected", ToastLength.Long).Show();
                }

                catch (System.Exception e)
                {
                    System.Console.Write(e.Data);
                }
            }
            else
            {
                Toast.MakeText(this, "Network is not connected", ToastLength.Long).Show();
                return false;
            }

            return false;
        }



    }
}

